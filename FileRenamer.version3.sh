#!/bin/bash

# set -x

# path2="/data/hu_qian/BashScripts/DateinamenSanieren/"

path="/tmp/"

path2="/tmp/"


############################ Config File content ######################################################
Eof()
{
cat << EOF
maximum_depth 	(Y/N) 	.................	Y		# if N is selected, the changes will only be applied to the current directory and not for subdirectories
directories	(Y/N) 	.................	Y		# if Y is selected, not only filenames, but invalid names of directories will also be changed
		#	---------------->	deleted		 
		*	---------------->	deleted		 
		$	---------------->	deleted		 
		[]	---------------->	deleted
		\	---------------->	deleted		
		^	---------------->	deleted		 
		|	---------------->	deleted		 
		~	---------------->	deleted
		"	---------------->	deleted
		%	---------------->	deleted
		&	---------------->	deleted
		:	---------------->	deleted
		<>	---------------->	deleted
		?	---------------->	deleted
		{}	---------------->	deleted
		|	---------------->	deleted
		()	---------------->	deleted
		blank	---------------->	_
		ÄäÖöÜü	---------------->	Ae/ae/Oe/oe/Ue/ue
		jpeg	---------------->	jpg
		PDF	---------------->	pdf
EOF
}

############################## Does FileRenamer.config exist? #########################################

ConfigCheck()
{
if ! find . -maxdepth 1 | grep FileRenamer.config
then
touch FileRenamer.config
Eof > FileRenamer.config
fi
}

############################## The user interface begins ###############################################

Interface()
{
echo "FileRenamer - an interface to rename files with invalid filenames"
echo "When executed, this program will make multiple changes to the filenames under your current directory."

read -n 1 -s -r -p "Press any key to continue"
echo ""
echo ""
echo ""

Eof  ####### Showing the content of the Config-File

echo ""
read -p "Do you want to keep this configuration? (Y/n) " ok


if [ "$ok" = "n" ]			# If you chose the configuration being not ok, then the config.-file will be opened immediately for making changes
then
gedit ./FileRenamer.config &
fi

echo ""
read -n 1 -s -r -p "Press any key to continue"
echo ""
}
#################################### The program recognizes the selection state of its 2 key options ###############################

Selections()
{

line="$(awk "NR==1" ./FileRenamer.config)"
change=$(echo "$line" |  awk '{print $4}')
if [ "$change" = "Y" ]
then
recursive="y"
else
recursive="n"
fi




line="$(awk "NR==2" ./FileRenamer.config)"
change=$(echo "$line" |  awk '{print $4}')
if [ "$change" = "Y" ]
then
directories="y"
else
directories="n"
fi

}

##################### Delete last character #######################

deleteLast()
{
a=$(cat /tmp/fileFindingSource)
echo -n "$a" | sed 's/.$//' > /tmp/fileFindingSource
}



########### The program needs to know the number of lines in its config.-file for later for-loops ###################

ConfigLineCount()
{
numberOfLines=$(wc -l $destination)
numberOfLines=$(echo $numberOfLines | awk '{ print $1 }')
}


ConfigLineCount2()
{
numberOfLines2=$(wc -l $destination2)
numberOfLines2=$(echo $numberOfLines2 | awk '{ print $1 }')
}

##################### Iterate through every character ##############################################################

CharacterCheck()
{

destination2="/tmp/original"
ConfigLineCount2


for ((lineNumber2=1;lineNumber2<numberOfLines2;lineNumber2+=1))
do
	
	char="$(awk "NR==$lineNumber2" $destination2)"

        # display one character at a time
	if echo "$char" | egrep "\*|\?|\[|\^|\(|\|" > /dev/null 2>&1
	then	
		echo -n "\\$char|" >> /tmp/fileFindingSource
		
	elif echo "$char" | egrep "\"" > /dev/null 2>&1
	then
		echo -n '\\\"|' >> /tmp/fileFindingSource
	

	elif echo "$char" | egrep "\\$" > /dev/null 2>&1
	then
		echo -n "\\\\$char|" >> /tmp/fileFindingSource
		

	elif echo "$char" | egrep "\\\\" > /dev/null 2>&1
	then
		echo -n "\\\\\\$char|" >> /tmp/fileFindingSource
		
	
	else
		echo -n "$char|" >> /tmp/fileFindingSource
		
	fi
done

}


CharacterChange()
{

destination2="/tmp/original"
ConfigLineCount2

for ((lineNumber2=1;lineNumber2<numberOfLines2;lineNumber2+=1))
do
	char="$(awk "NR==$lineNumber2" $destination2)"

	if echo "$char" | egrep "\"|\*|\?|\[|\^" > /dev/null 2>&1
	then	
		echo -n " | sed \"s/\\$char/$change/g\"" >> /tmp/renamerScriptSource
	
	elif echo "$char" | egrep "\\$" > /dev/null 2>&1
	then
		echo -n " | sed \"s/\\\\$char/$change/g\"" >> /tmp/renamerScriptSource

	elif echo "$char" | egrep "\\\\" > /dev/null 2>&1
	then
		echo -n " | sed \"s/\\\\\\$char/$change/g\"" >> /tmp/renamerScriptSource

	else
		echo -n " | sed \"s/$char/$change/g\"" >> /tmp/renamerScriptSource
	fi

done

}

######################### Generating search properties from Config-file into a textfile #############################################################

GenerateFindings()
{

echo -n "findings=\"\\\"" > /tmp/fileFindingSource

for ((lineNumber=3;lineNumber<=numberOfLines;lineNumber+=1))
do								
	line="$(awk "NR==$lineNumber" ./FileRenamer.config)"
	original=$(echo "$line" |  awk '{print $1}')
		
	if [ "$original" = "blank" ]
	then
		original=" "
		echo -n "$original|" >> /tmp/fileFindingSource
	elif [ "$original" = "jpeg" ]
	then
		echo -n "jpeg|" >> /tmp/fileFindingSource
	elif [ "$original" = "PDF" ]
	then
		echo -n "PDF|" >> /tmp/fileFindingSource
	else
		echo "$original" | sed -e 's/\(.\)/\1\n/g' > /tmp/original
		CharacterCheck
	fi
	
		
done

deleteLast

echo -n "\"\\\"" >> /tmp/fileFindingSource

source /tmp/fileFindingSource			# Saving the search properties from the textfile back into a variable

echo "$findings"

}


####################### Generating renaming algorithm from Config-file into a textfile #######################################

GenerateRenamings()
{
echo -n "echo \"\$old\"" > /tmp/renamerScriptSource

for ((lineNumber=3;lineNumber<=numberOfLines;lineNumber+=1))
do								
	line="$(awk "NR==$lineNumber" ./FileRenamer.config)"
	original=$(echo "$line" |  awk '{print $1}')
	change=$(echo "$line" |  awk '{print $3}')
	
	if echo "$change" | egrep "\"|\*|\?|\[|\^|\(|\||\\$|\\\\|!|#|%|&|)|:|;|<|=|>|@|]|{|}|~|'" > /dev/null 2>&1
	then
	echo ""	
	echo "WARNING"	
	echo "$line:    the symbol $change is forbidden on the right side"
	exit
	 
		
	elif [ "$change" = "deleted" ]
	then
		change=""
	

	elif [ "$original" = "blank" ]
	then
		original=" "
	

	elif [ "$change" = "blank" ]
	then
		change=" "
	
	fi

	if [ "$original" = "ÄäÖöÜü" ]
	then
		echo -n " | sed \"s/Ä/Ae/g\"" >> /tmp/renamerScriptSource
		echo -n " | sed \"s/ä/ae/g\"" >> /tmp/renamerScriptSource
		echo -n " | sed \"s/Ö/Oe/g\"" >> /tmp/renamerScriptSource
		echo -n " | sed \"s/ö/oe/g\"" >> /tmp/renamerScriptSource
		echo -n " | sed \"s/Ü/Ue/g\"" >> /tmp/renamerScriptSource
		echo -n " | sed \"s/ü/ue/g\"" >> /tmp/renamerScriptSource
	
	elif [ "$original" = "jpeg" ]
	then
		echo -n " | sed \"s/jpeg/$change/g\"" >> /tmp/renamerScriptSource

	elif [ "$original" = "PDF" ]
	then
		echo -n " | sed \"s/PDF/$change/g\"" >> /tmp/renamerScriptSource	
	
	else
		echo "$original" | sed -e 's/\(.\)/\1\n/g' > /tmp/original
		CharacterChange
	
	fi
		
done

echo -n " > /tmp/newFilename" >> /tmp/renamerScriptSource
}
	


################### Date and Time function ######################

getDate ()

{
	echo "" >> "${path2}LogfileOfRenamed"	
	d=`date +%Y-%m-%d-%H-%M-%S`
	echo $d >> "${path2}LogfileOfRenamed"
	echo "" >> "${path2}LogfileOfRenamed"
}


################## Log entry function ##############################################


logEntry()
{
echo "$oldName" >> "${path2}LogfileOfRenamed"
echo "has been renamed to: " >> "${path2}LogfileOfRenamed"
echo "$newName" >> "${path2}LogfileOfRenamed"
echo "" >> "${path2}LogfileOfRenamed"
}


##################### Actual renaming function ##########################################

actualRenaming()
{
mv -i "$oldName" "$newName"
}

#################### A function to avoid filename dupilcations ##################################

duplicationAvoider ()
{

if find | grep -w "$newName" 
then
newName="${newName}.renamed"
fi

}


##########################################################################################################

lineEndChanger() # because the sed command cannot focus only on the last part of a string, we must reverse the whole string, catch the first part, reverse it, replace it and then reverse the whole string back to normal
{

line="$*"


old=$(echo "$line" | awk -F/ '{print $NF}')


source /tmp/renamerScriptSource	
new=`cat /tmp/newFilename`	


newLine=$(echo "${line%/*}")

newName="$newLine/$new"

echo "$newName"

}



######################## Non-Recursive File renaming function ##################################

simpleRenamer ()
{
getDate

destination="/tmp/renamerLogfile"
ConfigLineCount
for ((lineNumber=1;lineNumber<=numberOfLines;lineNumber+=1))
do								
	line="$(awk "NR==$lineNumber" "${path}renamerLogfile")"
		
	if [ $recursive = "n" ]
	then
	old="$line"
	oldName="$line"	
	source /tmp/renamerScriptSource	
	newName=`cat /tmp/newFilename`
	elif [ $recursive = "y" ]
	then
	oldName="$line"	
	newName=$(lineEndChanger "$line")
	fi	
	
	duplicationAvoider
	logEntry	
	actualRenaming

done

echo "" >> "${path2}LogfileOfRenamed"

}



########### A function that catches only invalid filenames, but not invalid directory names ##############
########### if the option "directory" is selected as NO ################
########### It will catch all suspicious pathways and then filter them in a firstpass, only catching the files #######

fileOnlyFunc() 
{
destination="firstpassLogfile"
ConfigLineCount
for ((lineNumber=1;lineNumber<=numberOfLines;lineNumber+=1))
do								
	line="$(awk "NR==$lineNumber" ${path}firstpassLogfile)"
	fileName=$(echo "$line" | awk -F/ '{print $NF}')
	fileName=$(echo "$fileName" | egrep "$findings")
	if [ "$fileName" != "" ]
	then
	echo "$line" >> ${path}renamerLogfile
	fi
done

}


################ The recursive function for first changing directory names #######################
################ before changing file names ######################################################


findAndRename ()
{

# Finding any pathways containing forbidden special characters
find | egrep "$findings" > "${path}renamerLogfile"


# Sorting the pathways by length
cat "${path}renamerLogfile" | awk '{ print length, $0 }' | sort -n | awk '{$1=""; print $0}' | cut -c2- > "${path}renamerLogfileSorted"


# Counting the absolute number of lines/equivalent to the absolute remaining number of files or directories to be changed
destination="/tmp/renamerLogfileSorted"
ConfigLineCount


# Condition to stop the recursive function
if [ $numberOfLines = 0 ]
then
echo "Recursive function has been executed."
echo "There are no (more) files with names worthy of correction."
echo "" >> "${path2}LogfileOfRenamed"
exit
fi


# Computing the renaming of only the first line
oldName="$(awk "NR==1" "${path}renamerLogfileSorted")"
old="$(awk "NR==1" "${path}renamerLogfileSorted")"
source /tmp/renamerScriptSource
newName=`cat /tmp/newFilename`


# Saving the renaming process details into a logfile
logEntry


# Executing the renaming on the real file or directory
actualRenaming


# Recursively calling the function itself
findAndRename

}





################ The different cases which will trigger different main programs #################

Options()
{
if [ $recursive = "n" ] && [ $directories = "n" ]
then
option="1"                      ######## Option 1 is looking only for files in the current directory
elif [ $recursive = "n" ] && [ $directories = "y" ]
then
option="2"                     ######## Option 2 is looking for files AND directories in the current directory
elif [ $recursive = "y" ] && [ $directories = "n" ]
then
option="3"			######## Option 3 is looking only for files: beginning from the current directory until maximum recursive
elif [ $recursive = "y" ] && [ $directories = "y" ]
then
option="4"			######## Option 4 is looking for files AND directories: beginning from the current directory until maximum recursive
fi

echo "You have chosen option $option"
}

################## Main program #####################################################

ConfigCheck
Interface
Selections
Options
destination="./FileRenamer.config"
ConfigLineCount
GenerateFindings
GenerateRenamings



case "$option" in
1)
	find . -maxdepth 1 -type f | egrep "$findings" > /tmp/renamerLogfile
	simpleRenamer
	;;

2)
	find . -maxdepth 1 | egrep "$findings" > /tmp/renamerLogfile
	simpleRenamer
	;;

3)
	find . -type f | egrep "$findings" > /tmp/firstpassLogfile
	echo -n "" > ${path}renamerLogfile 				# Erasing former entries from the renamerLogfile
	fileOnlyFunc
	simpleRenamer
	;;

4)
	getDate	
	findAndRename
	echo "" >> ${path2}LogfileOfRenamed
	;;
esac
